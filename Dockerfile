FROM python:3.6-slim-jessie
RUN apt-get -y update && apt-get -y install git
RUN pip install pymysql git+https://github.com/Rapptz/discord.py@rewrite

ADD . /

CMD [ "python", "./src/main.py" ]

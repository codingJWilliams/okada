![Okada Banner](assets/okada.png)

## Okada - the discord utility bot

### Features

- NEW: Sharable Roles
  - Members can own roles that they can share out to their friends - see the [admin guide](#admin-guide)



### Guide

##### Command usage

Every command takes a certain number of arguments, seperated by spaces. Each argument can be an ID, a name, or a mention. If you need to have spaces inside of an argument you can, simply by using quotation marks. Let's look at an example of the `$roleadmin add <member> <role>` command.  
If you wanted to add the role `Clout Gang` to the user `Mama Nox` you would need quotations as each of those has multiple words, so the correct command would be `$roleadmin add "Mama Nox" "Clout Gang"`. However, if you used the IDs you would not need quotes as the ID itself has no spaces so `$roleadmin add 328668492586811393 248658492586811395` would work, as long as the IDs are correct. Addionally, mentions do not need quotes so the command `$roleadmin add @Mama Nox#3920 "Clout Gang"` would be valid syntax.

##### Sharable roles

Sharable roles are a great feature to allow members to build relationships, have fun colors on their name and have unique roles limited to their friend group only - they create a sense of exclusivity. Adding and removing roles is incredibly easy; just use the following two commands:  

- `$roleadmin add <member> <role>` - Gives ownership of the role to a member - Requires ADMINISTRATOR permission.
- `$roleadmin remove <member> <role>` - Removes their ownership of the role, however does not delete the role / remove the role from them - Requires ADMINISTRATOR permission.
- `$role give <member> <role>` - Allows a role owner to give their role to a friend
- `$role take <member> <role>` - Allows a role owner to take their role from a friend

import discord
from discord.ext import commands
import pymysql.cursors
import asyncio
def getConfig(key):
    with open("./secrets/" + key) as f: return f.read()

bot = commands.Bot("$", description="A bot to manage sharable roles")

def getsql():
    sql = pymysql.connect(
        host = getConfig("sql_ip"),
        user = getConfig("sql_user"),
        password = getConfig("sql_password"),
        db = getConfig("sql_database"),
        cursorclass=pymysql.cursors.DictCursor
    )
    sql.autocommit(True)
    return sql

@bot.event
async def on_ready():
    print("Loaded!")
    """
    with sql.cursor() as cursor:
        cursor.execute(
            "INSERT INTO `sharableroles` (`role_id`, `member_id`, `guild_id`) VALUES (%s, %s, %s)",
            ("129377388", "75636", "julieann")
        )
    sql.commit()
    """


@bot.group()
@commands.has_permissions(administrator = True)
async def roleadmin(ctx):
    if ctx.invoked_subcommand is None:
        await ctx.send('Invalid argument, see $help')

@roleadmin.command()
async def add(ctx: commands.Context, member: discord.Member, role: discord.Role):
    sql = getsql()
    with sql.cursor() as cursor:
        cursor.execute(
            "SELECT count(*) FROM `sharableroles` WHERE `role_id`=%s AND `member_id`=%s AND `guild_id`=%s",
            (role.id, member.id, ctx.guild.id)
        )
        value = cursor.fetchone()["count(*)"]
        if value != 0:
            await ctx.send(embed = discord.Embed(
                title = "That is already owned by them!",
                color = 0x2B2540,
                description = "You can remove it with `$roleadmin remove \"" + member.name + "\" \"" + role.name + "\"`."
            ))
            cursor.close()
            sql.close()
            return
        
        cursor.execute(
            "INSERT INTO `sharableroles`(`role_id`, `member_id`, `guild_id`) VALUES (%s, %s, %s)",
            (str(role.id), str(member.id), str(ctx.guild.id))
        )
        await ctx.send(embed = discord.Embed(
            title = "<:yay:433026009298632704> Role ownership added <:yay2:433026018790342665>",
            color = 0x2B2540,
            description = "They can share the role with `$role give @TheirFriend \"" + role.name + "\"`."
        ))
    sql.close()

@roleadmin.command()
async def remove(ctx: commands.Context, member: discord.Member, role: discord.Role):
    sql = getsql()
    with sql.cursor() as cursor:
        cursor.execute(
            "SELECT count(*) FROM `sharableroles` WHERE `role_id`=%s AND `member_id`=%s AND `guild_id`=%s",
            (role.id, member.id, ctx.guild.id)
        )
        value = cursor.fetchone()["count(*)"]
        if value == 0:
            await ctx.send(embed = discord.Embed(
                title = "They do not own that role!",
                color = 0x2B2540,
                description = "You can add it with `$roleadmin add \"" + member.name + "\" \"" + role.name + "\"`."
            ))
            cursor.close()
            sql.close()
            return
        
        cursor.execute(
            "DELETE FROM `sharableroles` WHERE `role_id`=%s AND `member_id`=%s AND `guild_id`=%s",
            (str(role.id), str(member.id), str(ctx.guild.id))
        )
        await ctx.send(embed = discord.Embed(
            title = "<:yay:433026009298632704> Role ownership removed <:yay2:433026018790342665>",
            color = 0x2B2540,
            description = "They can no longer share the role however the role itself has not been deleted."
        ))
    sql.close()
        
@bot.group()
async def role(ctx):
    if ctx.invoked_subcommand is None:
        await ctx.send('Invalid argument, see $help')
    

@role.command()
async def give(ctx: commands.Context, member: discord.Member, role: discord.Role):
    sql = getsql()
    with sql.cursor() as cursor:
        cursor.execute(
            "SELECT count(*) FROM `sharableroles` WHERE `role_id`=%s AND `member_id`=%s AND `guild_id`=%s",
            (role.id, ctx.author.id, ctx.guild.id)
        )
        value = cursor.fetchone()["count(*)"]
        if value == 0:
            await ctx.send(embed = discord.Embed(
                title = "That is not owned by you!",
                color = 0x2B2540,
                description = "Please ask an Administrator to add the role to you."
            ))
            cursor.close()
            sql.close()
            return
        
        await member.add_roles(role)
        await ctx.send(embed = discord.Embed(
            title = "<:yay:433026009298632704> Role added <:yay2:433026018790342665>",
            color = 0x2B2540,
            description = member.mention + " now has the role " + role.mention + "."
        ))
    sql.close()
@role.command()
async def take(ctx: commands.Context, member: discord.Member, role: discord.Role):
    sql = getsql()
    with sql.cursor() as cursor:
        cursor.execute(
            "SELECT count(*) FROM `sharableroles` WHERE `role_id`=%s AND `member_id`=%s AND `guild_id`=%s",
            (role.id, ctx.author.id, ctx.guild.id)
        )
        value = cursor.fetchone()["count(*)"]
        if value == 0:
            await ctx.send(embed = discord.Embed(
                title = "That is not owned by you!",
                color = 0x2B2540,
                description = "Please ask an Administrator to add the role to you."
            ))
            cursor.close()
            sql.close()
            return
        
        await member.remove_roles(role)
        await ctx.send(embed = discord.Embed(
            title = "<:yay:433026009298632704> Role taken. <:yay2:433026018790342665>",
            color = 0x2B2540,
            description = member.mention + " no longer has the role " + role.mention + "."
        ))
    sql.close()

bot.run(getConfig("token"))
